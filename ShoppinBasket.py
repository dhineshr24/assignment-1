import dataclasses


@dataclasses.dataclass()
class Item(object):
    unit_price = float
    quantity = 1

@dataclasses.dataclass
class Basket(object):
    items: list[Item]

    def total(self):
        if len(self.items) > 0:
            return self.items[0].unit_price
        return 0
